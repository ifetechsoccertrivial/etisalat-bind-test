var smpp = require('smpp');
var session = smpp.connect({host:'10.71.83.55', port: 4851});

session.bind_transceiver({
			command_id: '0x00000065',
            command_status: 0,
            sender_id: '0xFFFFFFFF',
            receiver_id: '0xFFFFFFFF',
            system_id: 'Terragon',
            password: 'Terragon',
            system_type: 'USSD',
            interface_version: '0x00000010'
}, function(pdu) {
	if (pdu.command_status == 0) {
		// Successfully bound
		session.submit_sm({
			destination_addr: '+2349036671876',
			short_message: 'Hello!'
		}, function(pdu) {
			if (pdu.command_status == 0) {
				// Message successfully sent
				console.log(pdu.message_id);
			}
		});
	}
});